
from bliss.setup_globals import *

load_script("mcl.py")

print("")
print("Welcome to your new 'mcl' BLISS session !! ")
print("")
print("You can now customize your 'mcl' session by changing files:")
print("   * /mcl_setup.py ")
print("   * /mcl.yml ")
print("   * /scripts/mcl.py ")
print("")